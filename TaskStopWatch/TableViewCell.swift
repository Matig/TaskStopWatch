//
//  TableViewCell.swift
//  TaskStopWatch
//
//  Created by Mati on 09.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    var timer = Timer()
    var hours: Int = 0
    var minutes: Int = 0
    var seconds: Int = 0
    var fractions: Int = 0

    var stopWatchString = ""
    
    var startStopWatch: Bool = true
    
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var stopWatchTimerLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!

    
    @IBAction func startStop(_ sender: Any) {
        if startStopWatch == true {
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateStopWatch), userInfo: nil, repeats: true)
            startStopWatch = false
            startStopButton.setImage(UIImage(named: "pause.png"), for: UIControlState.normal)
        } else {
            timer.invalidate()
            startStopWatch = true
            startStopButton.setImage(UIImage(named: "play.png"), for: .normal)
        }
    }
    
    @IBAction func reset(_ sender: Any) {
        
        fractions = 0
        seconds = 0
        minutes = 0
        hours = 0
        
        stopWatchString = "000:00:00.00"
        stopWatchTimerLabel.text = stopWatchString
    }
    
    func updateStopWatch() {
        
        fractions += 1
        if fractions == 100 {
            seconds += 1
            fractions = 0
        }
        
        if seconds == 60 {
            minutes += 1
            seconds = 0
        }
        
        if minutes == 60 {
            hours += 1
            minutes = 0
        }
        
        let fractionsString = fractions > 9 ? "\(fractions)" : "0\(fractions)"
        let secondsString = seconds > 9 ? "\(seconds)" : "0\(seconds)"
        let minutesString = minutes > 9 ? "\(minutes)" : "0\(minutes)"
        let hourString = hours > 9 ? "\(hours)" : "0\(hours)"
        
        stopWatchString = "\(hourString):\(minutesString):\(secondsString).\(fractionsString)"
        
        stopWatchTimerLabel.text = stopWatchString
    }

    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
