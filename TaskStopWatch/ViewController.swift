//
//  TableViewController.swift
//  TaskStopWatch
//
//  Created by Mati on 09.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import UIKit
import RealmSwift

protocol ViewControllerDelegate {
    func onClicked();
}

class ViewController: UITableViewController {
    
    var allTasks : [Task] = []
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        reloadTableView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    func reloadTableView() {
        let realm = try! Realm()
        allTasks = realm.objects(Task.self).sorted(by: { return $0.date.timeIntervalSince1970 < $1.date.timeIntervalSince1970 })
        tableView?.reloadData()
    }
    
    func deleteRowAtIndexPath(indexPath: IndexPath) {
        let realm = try! Realm()
        let listToBeDeleted = allTasks[indexPath.row]
        try! realm.write({ () -> Void in
            realm.delete(listToBeDeleted)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addTask" {
            let viewController = segue.destination as! AddTaskVC
            viewController.delegate = self
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTasks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : TableViewCell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TableViewCell
        let currentTask = allTasks[indexPath.row]
        
        cell.projectNameLabel.text = currentTask.projectName
        cell.taskLabel.text = currentTask.taskName
        cell.dateLabel.text = currentTask.dateLabel
        cell.backgroundColor = UIColor().HexToColor(hexString: currentTask.color, alpha: 1.0)
        return cell
    }
    
    // Left swipe
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            deleteRowAtIndexPath(indexPath: indexPath)
            reloadTableView()
        }
    }
}


extension ViewController: ViewControllerDelegate {
    internal func onClicked() {
        reloadTableView()
    }
}

extension UIColor{
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}

