//
//  realmObject.swift
//  TaskStopWatch
//
//  Created by Mati on 13.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import Foundation
import RealmSwift

class Task : Object {
    
    dynamic var projectName = ""
    dynamic var taskName = ""
    dynamic var dateLabel = ""
    dynamic var date = NSDate()
    dynamic var color = ""
    
}
