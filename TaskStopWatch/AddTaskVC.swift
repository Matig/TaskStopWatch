//
//  AddTaskVC.swift
//  TaskStopWatch
//
//  Created by Mati on 10.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import UIKit
import RealmSwift

class AddTaskVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var colorPicker: UIPickerView!
    @IBOutlet weak var colorPickerBtn: UIButton!
    @IBOutlet weak var projectNameTextField: UITextField!
    @IBOutlet weak var taskNameTextField: UITextField!
    var delegate: ViewControllerDelegate?
    
    let colors = ["green", "blue", "yellow", "red", "orange", "pink", "grey"]
    var pickedColor = ""
    var hexColor = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorPicker.delegate = self
        colorPicker.dataSource = self
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func dismissButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func colorBtnPressed(_ sender: Any) {
        colorPicker.isHidden = false
    }
    
    func setHexColor() -> String {
        if(pickedColor == "green") {
            hexColor = "8DFF44"
        } else if(pickedColor == "blue") {
            hexColor = "0BCFFF"
        } else if(pickedColor == "yellow") {
            hexColor = "EEFF84"
        } else if(pickedColor == "red") {
            hexColor = "FF655C"
        } else if(pickedColor == "orange") {
            hexColor = "FFBD00"
        } else if(pickedColor == "pink") {
            hexColor = "FF98AA"
        } else if(pickedColor == "grey") {
            hexColor = "C8C8C8"
        } else {
            hexColor = "FFFFFF"
        }
        
        return hexColor
    }

    
    @IBAction func addTaskBtn(_ sender: Any) {
        
        var date = NSDate()
        
        let realm = try! Realm()
        
        let newTask = Task()
        newTask.projectName = projectNameTextField.text!
        newTask.taskName = taskNameTextField.text!
        newTask.date = date
        newTask.dateLabel = getCurrentDate()
        newTask.color = setHexColor()
        
        try! realm.write {
            realm.add(newTask)
        }
        
        delegate?.onClicked()
        dismiss(animated: true, completion: nil)
    }
    
    func getCurrentDate() -> String{
        let dates = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: dates)
        let month = calendar.component(.month, from: dates)
        let year = calendar.component(.year, from: dates)
        
        let currentDate = "\(day).\(month).\(year)"
        
        return currentDate
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return colors[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        colorPickerBtn.setTitle(colors[row], for: UIControlState())
        pickedColor = colors[row]
        colorPicker.isHidden = true
    }
}
